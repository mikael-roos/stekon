Stekarnas intresseföreing
=============================

[![pipeline badge](https://gitlab.com/mikael-roos/stekon/badges/main/pipeline.svg)](https://gitlab.com/mikael-roos/stekon/-/pipelines)

Webbplats för Steköarnas intresseförening.

* [Utvecklingsversion av webbplatsen](https://mikael-roos.gitlab.io/stekon/hem.html).
